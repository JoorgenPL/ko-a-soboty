#include <iostream>
using namespace std;

const int m=1000;
int n;
long double oplacalnosc[m][3];

void scal (int pocz, int kon1, int pocz1, int kon)
{
    int j=0, k=0;
    long double T1[kon1-pocz+1][3], T2[kon-pocz1+1][3];
    for (int i=pocz; i<=kon1; i++)
    {

        T1[j][0]=oplacalnosc[i][0];
        T1[j][1]=oplacalnosc[i][1];
        T1[j][2]=oplacalnosc[i][2];
        j++;
    }
    j=0;
    for (int i=pocz1; i<=kon; i++)
    {
        T2[k][0]=oplacalnosc[i][0];
        T2[k][1]=oplacalnosc[i][1];
        T2[k][2]=oplacalnosc[i][2];
        k++;
    }
    k=0;
    int i=pocz;
    while (j<kon1-pocz+1 && k<kon-pocz1+1)
    {
        if (T1[j][0]<=T2[k][0])
        {
            oplacalnosc[i][0]=T1[j][0];
            oplacalnosc[i][1]=T1[j][1];
            oplacalnosc[i++][2]=T1[j++][2];
        } else
        {
            oplacalnosc[i][0]=T2[k][0];
            oplacalnosc[i][1]=T2[k][1];
            oplacalnosc[i++][2]=T2[k++][2];
        }
    }
    if (j==kon1-pocz+1)
    {
        for (int q=k; q<kon-pocz1+1; q++)
        {
            oplacalnosc[i][0]=T2[q][0];
            oplacalnosc[i][1]=T2[q][1];
            oplacalnosc[i++][2]=T2[q][2];
        }
    } else
    {
        for (int q=j; q<kon1-pocz+1; q++)
        {
            oplacalnosc[i][0]=T1[q][0];
            oplacalnosc[i][1]=T1[q][1];
            oplacalnosc[i++][2]=T1[q][2];
        }
    }
}

void sortuj (int pocz, int kon)
{
    int pocz1, kon1;
    if (pocz!=kon)
    {
        kon1=(pocz+kon)/2;
        pocz1=kon1+1;
        sortuj (pocz, kon1);
        sortuj (pocz1, kon);
        scal (pocz, kon1, pocz1, kon);
    }
}

int main()
{
    int P;
    cin >> P >> n;
    long double W[n], C[n], zysk=0;
    int b=n-1;
    for (int i=0; i<n; i++)
    {
        cin >> W[i] >> C[i];
        oplacalnosc[i][0]=C[i]/W[i];
        oplacalnosc[i][1]=C[i];
        oplacalnosc[i][2]=W[i];
    }
    sortuj(0, n-1);
    while (P>0 && b>=0)
    {
        if (P>=oplacalnosc[b][2])
        {
            P-=oplacalnosc[b][2];
            zysk+=oplacalnosc[b][1];
            b--;
        } else
        {
            zysk+=oplacalnosc[b][1]*(P/oplacalnosc[b][2]);
            P=0;
        }
    }
    cout << zysk;
    return 0;
}
